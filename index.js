(function (swiper) {
    'use strict';
    var init, bind, imgInit, bgInit, getImg, imgWidth, imgPosition, headFadeIn, poetryPosition, bgColor, textFadeIn, music, showNextLabel,
        MAT_BEFORE, MAT_AFTER,
        length1, length2;
    MAT_BEFORE = {
        left: [
            [230, 446, 96, 207, 357, 593],
            [95, 425, 178, 242, 649, 536],
            [94, 200, 329, 164, 432, 572, 625],
            [96, 302, 466, 279, 433, 616],
            [223, 96, 170, 327, 435, 569],
            [240, 439, 95, 234, 339, 573]
        ],
        top: [
            [704, 713, 804, 880, 836, 811],
            [843, 802, 704, 946, 782, 938],
            [769, 708, 949, 866, 783, 916, 755],
            [789, 706, 716, 887, 833, 813],
            [706, 773, 861, 880, 794, 761],
            [706, 729, 798, 892, 840, 797]
        ]
    };
    MAT_AFTER = {
        left: [
            [543, 624, 543, 592, 513, 556],
            [561, 625, 469, 539, 615, 608],
            [542, 442, 529, 611, 616, 616, 611],
            [563, 537, 521, 621, 630, 618],
            [543, 622, 523, 606, 596, 556],
            [546, 626, 556, 600, 514, 584]
        ],
        top: [
            [707, 752, 804, 818, 863, 931],
            [708, 752, 773, 874, 855, 906],
            [707, 767, 868, 759, 805, 871, 931],
            [706, 765, 875, 772, 817, 887],
            [705, 754, 810, 812, 863, 938],
            [705, 746, 795, 811, 859, 922]
        ]
    };
    length1 = 6;
    length2 = 6;
    getImg = function (page, callback) {
        var img = document.querySelectorAll('.words')[page].querySelectorAll('img'),
            i;
        for (i = 0; i < img.length; i += 1) {
            callback(img[i], i);
        }
    };
    imgWidth = function (page, x) {
        getImg(page, function (element, index) {
            element.width *= x;
        });
    };
    imgPosition = function (page, type) {
        var MAT = (type === 'before') ? MAT_BEFORE : MAT_AFTER;
        getImg(page, function (element, index) {
            element.style.left = MAT.left[page][index] / 2 + 'px';
            element.style.top = MAT.top[page][index] / 2 - 258 - 64 + 'px';
        });
    };
    poetryPosition = function (page) {
        document.querySelectorAll('.poetry img')[page].style.marginTop = 0;
        document.querySelectorAll('.poetry')[page].style.opacity = 1;
    };
    headFadeIn = function (page) {
        document.querySelectorAll('.head')[page].style.opacity = 1;
    };
    bgColor = function (page, type) {
        var a = (type === 'before') ? 0 : 0.6;
        document.querySelectorAll('.swiper-slide')[page].style.backgroundColor = 'rgba(0,0,0,' + a + ')';
    };
    textFadeIn = function (page) {
        var text = document.querySelectorAll('.swiper-slide')[page].querySelectorAll('.text'),
            i;
        for (i = 0; i < text.length; i += 1) {
            text[i].style.opacity = 1;
        }
    };
    imgInit = function () {
        var i;
        for (i = 0; i < length1; i += 1) {
            imgWidth(i, 0.5);
            imgPosition(i, 'before');
        }
        setTimeout(function () {
            imgPosition(0, 'after');
            poetryPosition(0);
        }, 2000);
    };
    bgInit = function () {
        var i;
        bgColor(length1, 'before');
        for (i = length1 + 1; i < length1 + length2; i += 1) {
            bgColor(i, 'after');
        }
    };
    music = function () {
        var player = document.querySelectorAll('audio')[0],
            playerBtn = document.querySelectorAll('.btn-audio')[0];
        playerBtn.classList.toggle('play');
        if (playerBtn.classList.contains('play')) {
            player.play();
        } else {
            player.pause();
        }
    };
    showNextLabel = function (show) {
        var nextLabel = document.querySelectorAll('.next')[0];
        nextLabel.style.opacity = (show === true) ? 1 : 0;
    };
    bind = function () {
        var playerBtn = document.querySelectorAll('.btn-audio')[0],
            shareBtn = document.querySelectorAll('.btn-share')[0];
        playerBtn.onclick = music;
        shareBtn.onclick = function () {
            window.alert('微信点击右上角，分享给你的小伙伴吧');
        };
        swiper.on('tap', function () {
            var player = document.querySelectorAll('audio')[0];
            player.play();
        });
        swiper.on('slideChangeStart', function (swiper) {
            (function (i) {
                if (swiper.activeIndex < length1) {
                    setTimeout(function () {
                        imgPosition(i, 'after');
                        poetryPosition(i);
                    }, 2000);
                } else {
                    setTimeout(function () {
                        bgColor(i, 'after');
                    }, 500);
                    setTimeout(function () {
                        textFadeIn(i);
                    }, 1000);
                }
                if (swiper.activeIndex === length1 + length2 - 1) {
                    showNextLabel(false);
                } else {
                    showNextLabel(true);
                }
            }(swiper.activeIndex));
        });
    };
    init = function () {
        imgInit();
        bgInit();
        bind();
        music();
        document.querySelectorAll('.swiper-container')[0].style.opacity = 1;
    };
    window.onload = init;
}(window.swiper));
